## Strings in JavaScript

> JavaScript Strings are zero or more characters written inside quotes, used for storing and manipulating text.

### Declaring Strings

There are two ways to declare strings in JavaScript.

1. > Strings as Literals 

    To declare variables in JavaScript, you need to use the var, let, or const keyword. Whether it is a string or a number, use the var, let, or const keyword for its declaration. But for declaring a string variable as literal we have to put the string inside double quotes or single quotes

    For example: 

    ```
    let str = "This is a string tutorial";
    let string1 = 'We can also declare string using single quotes';
    ```
    You can use quotes inside a string, as long as they don't match the quotes surrounding the string:

    ```
    let str = "He is called 'Ram'";
    let str1 = 'She is called "Sita"';
    ```

    **Escape** 
    **Character**

    Because strings must be written within quotes, JavaScript will misunderstand this string:

    ```
    let str = "Do not use double "quotes" inside double quotes";
    ```

    The string will be chopped to "Do not use double ".

    The solution to avoid this problem, is to use the backslash escape character.

    The backslash escape character turns special characters into string characters:

    ```
    let text = "We are the so-called \"Vikings\" from the north.";
    ```

    The output for the above code will be:

    We are the so-called "Vikings" from the north.

    Other escape sequences are valid in JavaScript are:

    | Code  | Result | Description|
    | ------ |:---:| ------:|
    | \\'     | ' | Single Quote |
    | \\" | " | Double Quote |
    |\\\\ | \\     | Backslash |
    |\\b|       | Backspace 
    | \f|	|Form Feed
    |\n	 |   | New Line
    | \r | |Carriage Return
    |\t	| | Horizontal Tabulator
    |\v|	|Vertical Tabulator

2. > Strings as Objects

    Strings can also be defined as objects with the keyword `new` : 
    
    ``` 
    let str = new String("JavaScript is cool");
    ```

---

### String Methods and Properties:

1. length :

    The `length` property returns the length of a string:

    ``` 
    let str = "This is test string";
    let length = str.length;
    ```

    The above code returns output 19.

2.  slice() :

    `slice()` extracts a part of a string and returns the extracted part in a new string.

    The method takes 2 parameters: start position, and end position (end not included).

    ```
    let str = "String method and properties";
    let res = str.slice(3, 10);
    ```

    The above code returns output "ing met".

    If you omit the second parameter, the method will slice out the rest of the string:

    ```
    let str = "String method and properties";
    let res = str.slice(15);
    ```
    
    The above code returns output "nd properties";

    If a parameter is negative, the position is counted from the end of the string:

    ```
    let text = "Apple, Banana, Kiwi";
    let part = text.slice(-12);
    ```    

    The above code returns output "Banana, Kiwi".

3. substring() :

    `substring()` is similar to `slice()`.

    The difference is that start and end values less than 0 are treated as 0 in `substring()`.

    ```
    let str = "Apple, Banana, Kiwi";
    let part = str.substring(7, 13);
    ```

    The output for above code is "Banana";

    If you omit the second parameter, substring() will slice out the rest of the string.

4. substr() :

    `substr()` is similar to `slice()`.

    The difference is that the second parameter specifies the length of the extracted part.

    ```
    let str = "Apple, Banana, Kiwi";
    let part = str.substr(7, 6);
    ```

    The output for above code is "Banana";

    If you omit the second parameter, substr() will slice out the rest of the string.

    If the first parameter is negative, the position counts from the end of the string.

    ```
    let str = "Apple, Banana, Kiwi";
    let part = str.substr(-4);
    ```

    The output for above code is "Kiwi".

5.  replace() : 

    The `replace()` method replaces a specified value with another value in a string:

    ```
    let text = "Please visit Microsoft!";
    let newText = text.replace("Microsoft", "W3Schools");
    ```

    The output for the above code is "Please visit W3Schools!".

    By default, the replace() method replaces only the first match.

6.  ReplaceAll() :

    The `replaceAll()` method allows you to specify a regular expression instead of a string to be replaced.

    ```
    text = "I love dogs. Dogs are popular";
    text = text.replaceAll("dogs", "cats");
    text = text.replaceAll("Dogs", "Cats");
    ```

    The output for the above code is "I love cats. Cats are popular".

7.  toUpperCase() :
 
    A string is converted to upper case with `toUpperCase()`.

    ```
    let text1 = "Hello World!";
    let text2 = text1.toUpperCase();
    ```

    The output for the above code is "HELLO WORLD!".

8.  toLowerCase():

    A string is converted to lower case with `toLowerCase()`.

    ```
    let text1 = "Hello World!";  
    let text2 = text1.toLowerCase();
    ```

    The output for the above code is "hello world!". 
    

9.  concat() :

    joins two or more strings:

    ```
    let text1 = "Hello";
    let text2 = "World";
    let text3 = text1.concat(" ", text2);
    ---
    The output for the above code is "Hello World!".

10. trimStart() :

    It removes the whitespace only from the start of a string.

    





    


