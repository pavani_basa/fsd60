const animatedImage1 = document.getElementById('diya1');
const animatedImage2 = document.getElementById('diya2');
const animatedImage3 = document.getElementById('diya3');
const animatedImage4 = document.getElementById('diya4');
const animatedImage5 = document.getElementById('diya5');
const animatedImage6 = document.getElementById('diya6');
let f1 = 0;
let f2 = 0;
let f3 = 0;
let f4 = 0;
let f5 = 0;
let f6 = 0;
var count = 0;
const totalFrames = 11;
function lightDiya(animatedImage) {
    if (!animatedImage.classList.contains('lit')) {
        animatedImage.classList.add('lit');
        count = count + 1;
        if (count === 6) {
            check();
        }
    }
}

function light1() {
    lightDiya(animatedImage1);
    animatedImage1.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f1}.svg`; 
    f1 = (f1 % totalFrames) + 1;
    requestAnimationFrame(() => light1());
}
function light2() {
    lightDiya(animatedImage2);
    animatedImage2.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f2}.svg`; 
    f2 = (f2 % totalFrames) + 1;
    requestAnimationFrame(() => light2());
}
function light3() {
    lightDiya(animatedImage3);
    animatedImage3.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f3}.svg`; 
    f3 = (f3 % totalFrames) + 1;
    requestAnimationFrame(() => light3());
}
function light4() {
    lightDiya(animatedImage4);
    animatedImage4.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f4}.svg`; 
    f4 = (f4 % totalFrames) + 1;
    requestAnimationFrame(() => light4());
}
function light5() {
    lightDiya(animatedImage5);
    animatedImage5.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f5}.svg`; 
    f5 = (f5 % totalFrames) + 1;
    requestAnimationFrame(() => light5());
    
}
function light6() {
    lightDiya(animatedImage6);
    animatedImage6.src = `https://www.google.com/logos/fnbx/diwali/diya_loop_${f6}.svg`; 
    f6 = (f6 % totalFrames) + 1;
    requestAnimationFrame(() => light6());
}


const stars = document.getElementById('stars');
let f = 0;

function screen() {
    stars.src = `https://www.google.com/logos/fnbx/diwali/desktop_stars_${f}.svg`; 
    f = (f % totalF) + 1;
    requestAnimationFrame(screen);
}
const totalF = 11;
const wic1 = document.getElementById("wick1");

wic1.addEventListener("mouseover", function() {
    wic1.classList.toggle("glow");
});
const wic2 = document.getElementById("wick2"); 
wic2.addEventListener("mouseover", function() {
    wic2.classList.toggle("glow");
});
const wic3 = document.getElementById("wick3"); 
wic3.addEventListener("mouseover", function() {
    wic3.classList.toggle("glow");
});
const wic4 = document.getElementById("wick4"); 
wic4.addEventListener("mouseover", function() {
    wic4.classList.toggle("glow");
});
const wic5 = document.getElementById("wick5"); 
wic5.addEventListener("mouseover", function() {
    wic5.classList.toggle("glow");
});
const wic6 = document.getElementById("wick6"); 
wic6.addEventListener("mouseover", function() {
    wic6.classList.toggle("glow");
});
function check() {
    if (count >= 5) {
            document.body.style.backgroundColor = "black";
            const stars = document.querySelectorAll('.star');
            stars.forEach(star => {
                star.classList.add('show');
            }); 
            const text = document.querySelector('.center-text');
            text.style.display = "none";
            const rs = document.querySelector('.rs');
            rs.style.display = "block";
            /*document.addEventListener("DOMContentLoaded", function() {
                document.write("nin");
               const videoElement = document.getElementById("gif");
                videoElement.currentTime = 3;
              
                videoElement.load();
                videoElement.play();
              
           
                setTimeout(function() {
                  videoElement.style.display = "none";
                }, 3000); 
              });*/
            const ran = document.getElementById('video-bg');
            ran.style.display = "block";
            const ts = document.getElementsByClassName('hide');
            for (const t of ts) {
                t.style.display = "block";
            }
            const aud = document.getElementById("audio");
            aud.play();
                  
        }
    } 
    
   
      